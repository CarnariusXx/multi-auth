<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
class AdminController extends Controller
{

    public function __construct(){
        $this->middleware('guest:admin');
    }
    // dashboard
    public function index(){
        return view('dashboard.index');
    }
    public function showAdminLoginform(){
        return view('dashboard.admin-login');
    }
    public function login(Request $request){
        // validate form data
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
        // attempt to login
        if( Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successfully then redirect to dashboard.
            return redirect()->intended(route('dashboard'));
        }
        // if unsuccessful then redrect back
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }
    public function register(){
        return view('dashboard.admin-register');
    }
}
