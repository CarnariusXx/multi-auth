<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/admin/login', 'AdminController@login')->name('admin-login');
// Route::get('/admin/register', 'AdminController@register')->name('admin-register');
// Route::get('/admin', 'AdminController@index')->name('dashboard');

Route::prefix('admin')->group(function(){
    Route::get('/', 'AdminController@index')->name('dashboard');
    Route::get('/login', 'AdminController@showAdminLoginform')->name('admin-login-form');
    Route::post('/login', 'AdminController@login')->name('admin-login');
    Route::get('/register', 'AdminController@register')->name('admin-register');
});
